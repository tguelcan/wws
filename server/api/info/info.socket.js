/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Info = require('./info.model');

exports.register = function(socket) {
  Info.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Info.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('info:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('info:remove', doc);
}