'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var InfoSchema = new Schema({
  title: String,
  infotext: String,
  active: Boolean,
  category: String,
  added: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Info', InfoSchema);
