'use strict';

var _ = require('lodash');
var Info = require('./info.model');
var Promise = require('bluebird');
var paginate = require('mongoose-pagination');

// Get list of infos
exports.index = function(req, res) {

  Promise.all([
    //Find All Datas
    Info.find()
      .paginate(req.query.page, 15)
      .sort('-added')
      .exec(),
    //Count all Datas in Collection
    Info.count().exec()
    //Find Datas without page filtering
  ]).spread(function (items, count) {
    //---------------------
    res.status(200).json({infos: items, count: count});
  }, function (err) {
    handleError(res, err);
  });
};

// Get a single info
exports.show = function(req, res) {
  Info.findById(req.params.id, function (err, info) {
    if(err) { return handleError(res, err); }
    if(!info) { return res.status(404).send('Not Found'); }
    return res.json(info);
  });
};

// Creates a new info in the DB.
exports.create = function(req, res) {
  Info.create(req.body, function(err, info) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(info);
  });
};

// Updates an existing info in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Info.findById(req.params.id, function (err, info) {
    if (err) { return handleError(res, err); }
    if(!info) { return res.status(404).send('Not Found'); }
    var updated = _.merge(info, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(info);
    });
  });
};

// Deletes a info from the DB.
exports.destroy = function(req, res) {
  Info.findById(req.params.id, function (err, info) {
    if(err) { return handleError(res, err); }
    if(!info) { return res.status(404).send('Not Found'); }
    info.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
