'use strict';

var express = require('express');
var controller = require('./resource.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/:id/getArticle', controller.getArticle);
router.get('/:id/getLocation', controller.getLocation);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
