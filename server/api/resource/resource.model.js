'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var ResourceSchema = new Schema({
  name: {type: String},
  info: {type: String},
  active: {type: Boolean, default: true},
  article : { type: Schema.Types.ObjectId, ref: 'Article' },
  location : { type: Schema.Types.ObjectId, ref: 'Location' },
  author : { type: Schema.Types.ObjectId, ref: 'User' },
  quota: { type: Number, default: 0 },
  count: { type: Number, default: 0 },
  added: { type: Date, default: Date.now }
});
ResourceSchema.plugin(deepPopulate);
ResourceSchema.index({ name: 'text', info: 'text', location: 'text', article: 'text', added: 'text', count: 'text'});


module.exports = mongoose.model('Resource', ResourceSchema);
