'use strict';

var _ = require('lodash');
var Resource = require('./resource.model');
var Promise = require('bluebird');
var paginate = require('mongoose-pagination');


// Get list of resources
exports.index = function (req, res) {

  if (req.query.search){
    var search = {$text: { $search : req.query.search }}
  }

  Promise.all([
    //Find All Datas
    Resource.find()
      .populate({path: 'article'})
      .populate({path: 'location'})
      .paginate(req.query.page, 15)
      .sort('-added')
      .exec(),
    //Count all Datas in Collection
    Resource.count().exec(),
    //Find Datas without page filtering
    Resource.find(search)
      .populate({path: 'article'})
      .populate({path: 'location'})
      .exec()
  ]).spread(function (items, count, quota) {
    //---------- Statistic for quota and count
    var allQuotas = _.map(quota, 'quota');
    var sumAllQuotas = _.reduce(allQuotas, function (accumulator, value) {
      return accumulator + value;
    });

    var allCounts = _.map(quota, 'count');
    var sumAllCounts = _.reduce(allCounts, function (accumulator, value) {
      return accumulator + value;
    });

    //---------------------
    res.status(200).json({resources: items, count: count, allquotas: sumAllQuotas, allcounts: sumAllCounts});
  }, function (err) {
    handleError(res, err);
  });

};

// Get a single resource
exports.show = function (req, res) {
  Resource.findById(req.params.id, function (err, resource) {
    if (err) {
      return handleError(res, err);
    }
    if (!resource) {
      return res.status(404).send('Not Found');
    }
    return res.json(resource);
  });
};


// Get Articles in Resources
exports.getArticle = function (req, res) {
  Promise.all([
    Resource.find()
      .populate({path: 'article'})
      .populate({path: 'location'})
      .sort('-added')
      .where('article').equals(req.params.id)
      .exec(),
    Resource.count()
      .where('article').equals(req.params.id)
      .exec()
  ]).spread(function (items, count) {

    //---------- Statistik
    var allQuotas = _.map(items, 'quota');
    var sumAllQuotas = _.reduce(allQuotas, function (accumulator, value) {
      return accumulator + value;
    });

    var allCounts = _.map(items, 'count');
    var sumAllCounts = _.reduce(allCounts, function (accumulator, value) {
      return accumulator + value;
    });
    //---------------------
    res.status(200).json({resources: items, count: count, allquotas: sumAllQuotas, allcounts: sumAllCounts});
  }, function (err) {
    handleError(res, err);
  });
};

// Get Articles in Resources
exports.getLocation = function (req, res) {
  Promise.all([
    Resource.find()
      .populate({path: 'article'})
      .populate({path: 'location'})
      .sort('-added')
      .where('location').equals(req.params.id)
      .exec(),
    Resource.count()
      .where('location').equals(req.params.id)
      .exec()
  ]).spread(function (items, count) {

    //---------- Statistik
    var allQuotas = _.map(items, 'quota');
    var sumAllQuotas = _.reduce(allQuotas, function (accumulator, value) {
      return accumulator + value;
    });

    var allCounts = _.map(items, 'count');
    var sumAllCounts = _.reduce(allCounts, function (accumulator, value) {
      return accumulator + value;
    });
    //---------------------
    res.status(200).json({resources: items, count: count, allquotas: sumAllQuotas, allcounts: sumAllCounts});
  }, function (err) {
    handleError(res, err);
  });
};

// Creates a new resource in the DB.
exports.create = function (req, res) {
  Resource.create(req.body, function (err, resource) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(resource);
  });
};

// Updates an existing resource in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Resource.findById(req.params.id, function (err, resource) {
    if (err) {
      return handleError(res, err);
    }
    if (!resource) {
      return res.status(404).send('Not Found');
    }
    var updated = _.merge(resource, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(resource);
    });
  });
};

// Deletes a resource from the DB.
exports.destroy = function (req, res) {
  Resource.findById(req.params.id, function (err, resource) {
    if (err) {
      return handleError(res, err);
    }
    if (!resource) {
      return res.status(404).send('Not Found');
    }
    resource.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
