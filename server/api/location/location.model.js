'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LocationSchema = new Schema({
  name: String,
  street: String,
  streetNumber: String,
  zip: String,
  city: String,
  country: String,
  lat: Number,
  long: Number,
  phone: String,
  info: String,
  active: { type: Boolean, default: true},
  added: { type: Date, default: Date.now }
});

LocationSchema.index({ name: 'text', street: 'text', city:'text', zip: 'text'});

/**
 * Pre-remove hook
 */
LocationSchema
  .pre('remove', true, function (next, done) {
    this.model('Resource').remove({ location: this._id }, next);
  });

module.exports = mongoose.model('Location', LocationSchema);
