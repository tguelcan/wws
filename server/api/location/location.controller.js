'use strict';

var _ = require('lodash');
var Location = require('./location.model');
var Promise = require('bluebird');
var paginate = require('mongoose-pagination');


// Get list of locations
exports.index = function(req, res) {
  if (req.query.search){
    var search = {$text: { $search : req.query.search }}
  }

  Promise.all([
    //Find All Datas
    Location.find(search)
      .paginate(req.query.page, 15)
      .sort('name')
      .exec(),
    //Count all Datas in Collection
    Location.count().exec()
    //Find Datas without page filtering
  ]).spread(function (items, count) {
    //---------------------
    res.status(200).json({locations: items, count: count});
  }, function (err) {
    handleError(res, err);
  });
};

// Get list of things
exports.getall = function(req, res) {
  Location.find(function (err, location) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(location);
  });
};

// Get a single location
exports.show = function(req, res) {
  Location.findById(req.params.id, function (err, location) {
    if(err) { return handleError(res, err); }
    if(!location) { return res.status(404).send('Not Found'); }
    return res.json(location);
  });
};

// Creates a new location in the DB.
exports.create = function(req, res) {
  Location.create(req.body, function(err, location) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(location);
  });
};

// Updates an existing location in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Location.findById(req.params.id, function (err, location) {
    if (err) { return handleError(res, err); }
    if(!location) { return res.status(404).send('Not Found'); }
    var updated = _.merge(location, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(location);
    });
  });
};

// Deletes a location from the DB.
exports.destroy = function(req, res) {
  Location.findOneAndRemove({_id: req.params.id}, function (err, doc) {
    doc.remove();
    if(err) { return handleError(res, err); }
    return res.status(204).send('No Content');
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
