'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ArticleSchema = new Schema({
  name: String,
  description: String,
  ingredients: String,
  info: String,
  active: { type: Boolean, default: true},
  added: { type: Date, default: Date.now }
});

ArticleSchema.index({ name: 'text', description: 'text', ingredients:'text'});

/**
 * Pre-save hook
 */
ArticleSchema
  .pre('save', function (next, done) {
      next();
  });

/**
 * Pre-remove hook
 */
ArticleSchema
  .pre('remove', true, function (next, done) {
    this.model('Resource').remove({ article: this._id }, next);
  });

module.exports = mongoose.model('Article', ArticleSchema);
