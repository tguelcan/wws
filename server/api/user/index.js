'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/addUser',auth.hasRole('admin'), controller.addUser);
router.put('/:id/editUser',auth.hasRole('admin'), controller.editUser);
router.put('/location',auth.isAuthenticated(), controller.changeLocation);
router.post('/', controller.create);

router.put('/:id/status', auth.isAuthenticated(), controller.changeStatus);
router.put('/:id/role', auth.isAuthenticated(), controller.changeRole);

module.exports = router;
