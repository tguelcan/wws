'use strict';

var user = require('./user.model');

exports.register = function(socket) {
    user.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    user.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};

function onSave(socket, doc, cb) {
  user.populate(doc, {path:'profile'}, function(err, user) {
    socket.emit('user:save', user);
  });
}


function onRemove(socket, doc, cb) {
    socket.emit('user:remove', doc);
}
