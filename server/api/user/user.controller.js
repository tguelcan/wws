'use strict';

var _ = require('lodash');
var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var Promise = require('bluebird');
var paginate = require('mongoose-pagination');

var validationError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {

  if (req.query.search){
    var search = {$text: { $search : req.query.search }}
  }

  Promise.all([
    //Find All Datas
    User.find(search)
      .paginate(req.query.page, 15)
      .sort('name')
      .exec(),
    //Count all Datas in Collection
    User.count().exec()
    //Find Datas without page filtering
  ]).spread(function (items, count) {
    //---------------------
    res.status(200).json({users: items, count: count});
  }, function (err) {
    handleError(res, err);
  });
};


/**
 * Creates a new user by admin add
 */
exports.addUser = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'guest';
  newUser.save(function (err, user) {
    if (err) return validationError(res, err);

    // var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});
    // res.json({token: token});

    res.json(200, user);
  });
};


// Updates an existing article in the DB.
exports.editUser = function(req, res) {
  //if(req.body._id) { delete req.body._id; }
  User.findById(req.body._id, function (err, user) {
    if (err) { return handleError(res, err); }
    if(!user) { return res.status(404).send('Not Found'); }
    var updated = _.merge(user, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(user);
    });
  });
};

/**
 * Creates a new user by signup
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'guest';
  newUser.save(function (err, user) {
    if (err) return validationError(res, err);

    // var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});
    // res.json({token: token});

    res.json(200, user);
  });
};


/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    res.json(user.profile);
  });
};


/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.status(500).send(err);
    return res.status(204).send('No Content');
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.status(200).send('OK');
      });
    } else {
      res.status(403).send('Forbidden');
    }
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.status(401).send('Unauthorized');
    res.json(user);
  });
};


/**
 * Change a users status
 */
exports.changeStatus = function (req, res, next) {

  User.findById(req.params.id, function (err, user) {

    if (err) { return handleError(res, err); }
    if(!user) { return res.status(404).send('Benutzer nicht gefunden.'); }

    if(user.active){
      user.active = false;
      user.role = 'guest';
      user.save(function (err, status) {
        if (err) return validationError(res, err);
        res.send(status).end();
      });
    }else if(!user.active){
      user.active = true;
      user.role = 'user';
      user.save(function (err, status) {
        if (err) return validationError(res, err);
        res.send(status).end();
      });
    }else {
      res.send(403);
    }

  });
};


/**
 * Switch a users role to admin
 */
exports.changeRole = function (req, res, next) {

  User.findById(req.params.id, function (err, user) {

    if (err) { return handleError(res, err); }
    if(!user) { return res.status(404).send('User Not Found'); }

    if(user.role == 'user'){
      user.role = 'admin';
      user.location = null;
      user.save(function (err) {
        if (err) return validationError(res, err);
        res.send(200).end();
      });
    }else if(user.role == 'admin'){
      user.role = 'user';

      user.save(function (err) {
        if (err) return validationError(res, err);
        res.send(200).end();
      });
    }else {
      res.send(403).end();
    }

  });
};

exports.changeLocation = function (req, res, next) {
  if(!req.user) { return res.status(404).send('Not Found'); }
  if(req.user.role == 'admin'){return res.status(403).send('Admin not allowed to set Location');}

  //SET LOCATION
  var userId = req.user._id;
  User.findById(userId, function (err, user) {
    var updated = _.merge(user, {location: req.body.id});
    updated.save(function (err) {
      console.log(user);
      return res.status(200).json(user);
    });
  });
};


/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};
