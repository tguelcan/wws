'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    //uri: 'mongodb://92.51.166.168:27017/wws-dev'
    uri: 'mongodb://localhost/wws-dev'
  },

  seedDB: false
};
