/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');
var Location = require('../api/location/location.model');
var Article = require('../api/article/article.model');
var Resource = require('../api/resource/resource.model');


Resource.find({}).remove(function () {
});

Article.find({}).remove(function () {
  Article.create([
    {
      name: "ANANAS KGB 7Stück ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "BANANEN  18kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "BERLINER SCH.VAN.TK B&B.36X82G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "BIRNEN LOW BUDGET 15kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Backkartoffel 10kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Balisto grün ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Balisto lila ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Balisto orange ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Becher 0,25",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Becher 0,35",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Blumenk.Käse.Stern 40 St ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Blutoxol Flächen und Bodenreiniger",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "CHICK.NUGG.GEG.TK      TGE.1KG",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "COOKIE CH.CHU.XL TK B&B.96X80G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "COOKIE TR.CH.XL TK  B&B.96X80G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "CURRYWURST GERÄU.  URS.10X160G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Croissont Frischkäse ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Croissont Nougat ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Croissont Schinken",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Croissont Schoko ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "DONUT KIDS CRUN.TK  B&B.12X57G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "DONUT SC.MILK.TK B&B.CA.12X56G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Deckel Flach für Dom ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Deli Magarine 500gr ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Dextro Apfel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Dextro Erdbeer ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Domdeckel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Duplo ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "EIERPF.KU.NAT.UNGES.TK TGQ.60G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Edamer Scheiben 1kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Einlegeschale für Obst ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Eisberg 10er Kiste ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "FILETSTÜCK.WOK VAL.VEG.TK  2KG",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Farfalle  5kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Fischstäbchen 30St ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Fladenbrot",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Frischkäse 200gr Kräuter ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Fusili  5kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "GEFL.WIENER WÜRST.      20X50G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "GEMÜS.SCHN.BACKOF.TK  FVZ.145G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Gemüsebouillon 12 Kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Germknödel 10er ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Glutenfrei Mehl ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Glutenfreie Lasagneplatten ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Gouda gerieben 1kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Granini Apfel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Granini Kirsch ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Granini Multi ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Grasset Fettlöser ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Gurke 12er",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "H-Banana Drink 0,1% 0,5L",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "H-ERDBEER DRINK  0,1%      MIL.0,5L",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "H-KAKAO BIS 0,3%      MIL.0,5L",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "H-VANILLA DRINK 0,1%    MIL. 0,5L",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "HAMB.BRÖT.SES.10CM TK  TGE.55G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Hamburger Patty 80St ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Handschuhe L ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Handschuhe M",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Hefe einzelner Würfel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "HotDog Gurken 770ml ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "HÄHN.CO.BL.PAN.TK     TGE.150G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "HÄHN.SCHNI.PAN.TK     TGE.140G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Hähnchen140gr 3kg TK ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Hähnchenbrust für DGE 10kg TK ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Joghurt Natur 5kg 0,1%",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "KAISERSEMMEL GLUTE.TK DRS.350G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "KART.1/4 GESCH.FESTK.10KG ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "KART.1/4 für Brei 10 kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Kartoffelpuffer 50St ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Kindercountry ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Kinderriegel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Kiwi Stück 130St Kiste ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Kohlrabi frisch Kiste",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Käselaugenstange",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Küchensahne 20%  1Ltr ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "LAUGENBREZEL TK        TGE.93G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Lasagneplatten",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Leere Sprühflsche zu Blutoxol",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MEHRKORNBRÖTCHEN TK    TGE.85G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MELONE HONIG GELB NGB 10Stck",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MINIS VALESS PAN.VEG.TK    25G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MM blau ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MM braun ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MM gelb ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MSC.BACKFI.FIL.I.BACKT.TK  75G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MUFFIN Choc.No. TK B&B 12X110G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "MUFFIN SCH.MILK.TK B&B.12X110G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Makkaroni   5kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Malteser ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos Apfel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos Cola ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos Duo ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos Erdbeer ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos Mint ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos Rainbow ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mentos frucht ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Milch 1lt 1,5% 1Ltr FRISCH",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mozzarella MiniKugeln Eimer",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mozzarella Stange 1kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Mozzarella kleine Kugel ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "PAPRIKA GELB NGB 5kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "PAPRIKA ROT NGB 5kg",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "PIZZABALL VALESS PAN.VEG.TK15G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "PUT.BR.GERÄ.GESCHN.KAL.10 500G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pickup Choco ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pickup Karamell ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pickup Milch ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pickup schwarz ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pizza Caprese 24 Stück",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pizza Salami 24 Stück ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pizzasauce 4250 ml",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Pute 140gr 3kg TK ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Putenbrust für DGE 15kg TK ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "RI.HACKFL.IQF HVO 6KG       TK",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Remoulade 5kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Rotkohl 10kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Röstzwiebeln 1kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "SCHLEMM.FIL.BORD.TK   TGE.200G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "SCHNITZEL VALESS PAN.VEG.TK75G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Salami Baguette 6er ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Schmelzkäse 150gr. Kräuter ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Schnelldesinfektion 1Ltr ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Skittles grün ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Skittles rot ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Speisequark 10!!!!kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Spätzle ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "TRUTH.SAL.GESCH.KAL.100   500G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Talos Feta Käse ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Tomaten 5-6 kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Tomatenmark 4250ml",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "VOLLEI FLÜSSIG BOHA    TGQ.1KG",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "VOLLEI FLÜSSIG BOHA   TGQ.10KG",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Vinoxin Edelstahlreiniger",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Vollkornsalami Baguette  2er",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "WEIZENBRÖTCHEN TK      TGE.70G",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "WEIZENTORTILL.GRILLED30CM 144ST",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Weisskohl 10kg ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "Xon-Forte Backofenreiniger ",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "ZAZIKI                 TGQ.3KG",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "vegi Bratwurst Weimar 40 St.",

      description: "",
      ingredients: "",
      count: ""
    },
    {
      name: "ÄPFEL GESCHÜ.NGB 10-15kg",

      description: "",
      ingredients: "",
      count: ""
    }
  ]);
});


Location.find({}).remove(function () {
  Location.create({
      name: 'Gymn. Raabeschule / IGS Heidberg',
      street: 'Stettinstraße 1',
      zip: '38124',
      city: 'Braunschweig',
      phone: '0531 4707600',
      info: '',
      active: true
    },
    {
      name: 'Raabeschule/Heidberg',
      street: 'Stettinstraße 1',
      zip: '38124',
      city: 'Braunschweig',
      phone: '0531 4707600',
      info: '',
      active: true
    },
    {
      name: 'Lessinggymnasium',
      street: 'Heideblick 20',
      zip: '38110',
      city: 'Braunschweig',
      phone: '05307 92150',
      info: '',
      active: true
    },
    {
      name: 'Nibelungen',
      street: 'Ortwinstraße 2',
      zip: '38112',
      city: 'Braunschweig',
      phone: '0531 230140',
      info: '',
      active: true
    },
    {
      name: 'Ricarda Huch',
      street: 'Mendelssohnstraße 6',
      zip: '38106',
      city: 'Braunschweig',
      phone: '0531 3870030',
      info: '',
      active: true
    },
    {
      name: 'Nibelungen-Realschule',
      street: 'Ortwinstraße 2',
      zip: '38112',
      city: 'Braunschweig',
      phone: '0531 230140',
      info: '',
      active: true
    },
    {
      name: 'Gebrüder-Grimm',
      street: 'Maschstraße 1',
      zip: '38518',
      city: 'Gifhorn',
      phone: '05371 16037',
      info: '',
      active: true
    },
    {
      name: 'Humboldtgymnasium',
      street: 'Fritz-Reuter-Straße 1',
      zip: '38518',
      city: 'Gifhorn',
      phone: '05371 98560',
      info: '',
      active: true
    },
    {
      name: 'Grundschule Adenbüttel',
      street: 'Gifhorner Str. 22,',
      zip: '38528',
      city: 'Gifhorn',
      phone: '05304 91410',
      info: '',
      active: true
    },
    {
      name: 'Gunzelin',
      street: 'Gunzelinstraße 42',
      zip: '31224',
      city: 'Peine',
      phone: '05171 7902710',
      info: '',
      active: true
    },
    {
      name: 'Am Silberkamp',
      street: 'Am Silberkamp 30',
      zip: '31224',
      city: 'Peine',
      phone: '05171 4019501',
      info: '',
      active: true
    },
    {
      name: 'Salzgitter Thiede',
      street: 'Panscheberg 56',
      zip: '38239',
      city: 'Salzgitter',
      phone: '05341 29300',
      info: '',
      active: true
    },
    {
      name: 'Schulzentrum Vechelde',
      street: 'Berliner Straße 45',
      zip: '38159',
      city: 'Vechelde',
      phone: '05302 806800',
      info: '',
      active: true
    },
    {
      name: 'Gymnasium im Schloss',
      street: 'Schloßpl. 13',
      zip: '38304',
      city: 'Wolfenbüttel',
      phone: '05331 92300',
      info: '',
      active: true
    },
    {
      name: 'Theodor Heuss',
      street: 'Karl-von-Hörsten-Straße 7-9',
      zip: '38304',
      city: 'Wolfenbüttel',
      phone: '05331 95630',
      info: '',
      active: true
    },
    {
      name: 'Wilhelm Raabe',
      street: 'Adersheimer Str. 60',
      zip: '38304',
      city: 'Wolfenbüttel',
      phone: '05331 90830',
      info: '',
      active: true
    },
    {
      name: 'Sickte',
      street: 'Schulweg 2',
      zip: '38173',
      city: 'Sickte',
      phone: '05305 91940',
      info: '',
      active: true
    }
  );
});

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test',
    active: true
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin',
    active: true

    }, function() {
      console.log('finished populating users');
    }
  );
});
