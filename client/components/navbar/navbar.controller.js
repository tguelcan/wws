'use strict';

angular.module('wwsApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth) {
    $scope.menuAdmin = [
      {
        'title': 'Übersicht',
        'link': '/resources'
      },
      {
        'title': 'Orte',
        'link': '/locations'
      },
      {
        'title': 'Artikel',
        'link': '/articles'
      },
      {
        'title': 'Benutzer',
        'link': '/admin'
      },
      {
        'title': 'Info',
        'link': '/info'
      }];

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function () {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function (route) {
      return route === $location.path();
    };
  });
