'use strict';

angular.module('wwsApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id/:controller', {
      id: '@_id'
    },
    {
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      },
      show: {
        method: 'GET'
      },
      changeStatus: {
        method: 'PUT',
        params: {
          controller:'status'
        }
      },
      changeRole: {
        method: 'PUT',
        params: {
          controller:'role'
        }
      },
      changeLocation: {
        method: 'PUT',
        params: {
          controller:'location'
        }
      },
      addUser: {
        method: 'Post',
        params: {
          controller:'addUser'
        }
      },
      editUser: {
        method: 'Put',
        params: {
          controller:'editUser'
        }
      }
	  });
  });
