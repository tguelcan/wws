'use strict';

describe('Directive: helper', function () {

  // load the directive's module
  beforeEach(module('wwsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<helper></helper>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the helper directive');
  }));
});