'use strict';

angular.module('wwsApp')
  .filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {

      var output = Math.round((input * 100)/ decimals);
      return $filter('number')(output);
    };
  }])
  .filter('percentageValue', ['$filter', function ($filter) {
    return function (input) {
      var type = 'default';

      if (input < 25) {
        type = 'danger';
      } else if (input < 50) {
        type = 'warning';
      } else if (input < 75) {
        type = 'warning';
      } else {
        type = 'success';
      }
      return type;
    };
  }]);
