'use strict';

angular.module('wwsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('impressum', {
        url: '/impressum',
        templateUrl: 'app/impressum/impressum.html',
        controller: 'ImpressumCtrl'
      });
  });
