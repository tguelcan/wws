'use strict';

describe('Service: relation.service', function () {

  // load the service's module
  beforeEach(module('wwsApp'));

  // instantiate service
  var resourceService;
  beforeEach(inject(function (_resourceService_) {
    resourceService = resource.service_;
  }));

  it('should do something', function () {
    expect(!!resourceService).toBe(true);
  });

});
