'use strict';

angular.module('wwsApp')
  .factory('Resource', function ($resource) {
      return $resource('/api/resources/:id/:controller', {
          id: '@_id'
        },
        {
          add: {
            method: 'POST'
          },
          get: {
            method: 'GET',
            isArray: false
          },
          getOne: {
            method: 'GET',
            isArray: false
          },
          getArticle: {
            method: 'GET',
            isArray: false,
            params: {
              controller:'getArticle'
            }
          },
          getLocation: {
            method: 'GET',
            isArray: false,
            params: {
              controller:'getLocation'
            }
          },
          delete: {
            method: 'DELETE'
          },
          update: {
            method: 'PUT'
          }
        });
    });
