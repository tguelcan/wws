'use strict';

angular.module('wwsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/resources',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        authenticate: true
      });
  });
