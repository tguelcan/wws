'use strict';

angular.module('wwsApp')
  .controller('MainCtrl',['$scope', 'Resource', 'socket', '$modal', 'Modal', function ($scope, Resource, socket, $modal, Modal) {
    //$scope.searchText = "";
    $scope.showStat = false;

    $scope.search = function (searchText){
      if (searchText || searchText == "") {
        $scope.searchText = searchText;
        getResource();
      }
    };

    //NEW Resource
    $scope.addResourceModal = function () {
      $modal.open({
        templateUrl: 'app/main/resourceAdd.html',
        size: 'lg',
        controller: 'ResourceModalCtrl',
        resolve: {
          param: function () {
            return null;
          }
        }
      });
    };

    //NEW Resource
    $scope.editResourceModal = function (param) {
      $modal.open({
        templateUrl: 'app/main/resourceEdit.html',
        size: 'lg',
        controller: 'ResourceModalCtrl',
        resolve: {
          param: function () {
            return param;
          }
        }
      });
    };
    $scope.resources = [];
    $scope.resources.allcounts = 0;
    $scope.resources.allquotas = 0;

    $scope.currentPage = 1;

    var getResource = function(){
      Resource.get({page: $scope.currentPage, search: $scope.searchText},function () {
      }).$promise.then(function (resources) {
        $scope.resources = resources;

        //-----------DONUT -----------//
        var stockDatasPercent = Math.round((resources.allcounts * 100) /  resources.allquotas);
        if(isNaN(stockDatasPercent)){var stockDatasPercent = 0;}
        $scope.stockDatas = [{label: "Lagerbestand", value: stockDatasPercent, color: "#772953", suffix: "%"}];
        $scope.stockOptions = {thickness: 5, mode: "gauge", total: 100};
        //----------------------------------//

        socket.syncUpdates('resource',  $scope.resources.resources);

      }, function (error) {
        console.log(error);
      });
    };

    //EXEC GET
    getResource();

    //Change PAGE
    $scope.changePage = function () {
      getResource();
      window.location.hash = '#top';
    };

    $scope.delete = Modal.confirm.delete(function (resource) { // callback when modal is confirmed
      Resource.delete({id: resource._id}, function () {
      }).$promise.then(function (resource) {
        socket.syncUpdates('resource',  $scope.resources.resources)
      }, function (error) {
        console.log(error);
      });
    });

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('resource');
    });
  }])
  .controller('ResourceModalCtrl',['$scope', 'Resource', 'Article', 'Location', '$modalInstance', 'param', function ($scope, Resource, Article, Location, $modalInstance, param) {
    $scope.resource = {};


    if(param){
      $scope.param = param;
    }

    Article.getall(function () {
    }).$promise.then(function (articles) {
      $scope.articles = articles;
    }, function (error) {
    });

    Location.getall(function (locations) {
    }).$promise.then(function (locations) {
      $scope.locations = locations;
    }, function (error) {
    });

    $scope.ok = function (resource) {
      //set quota
      resource.quota = resource.count;

      if(!resource.location._id || !resource.article._id){
        return
      }
      Resource.add(resource, function () {
      }).$promise.then(function () {
        $modalInstance.close();
      }, function (error) {
        console.log(error);
      });
    };


    $scope.edit = function (resource) {
      //set quota
      if(!resource.location._id || !resource.article._id){
        return
      }
      Resource.update(resource, function () {
      }).$promise.then(function () {
        $modalInstance.close();
      }, function (error) {
        console.log(error);
      });
    };




  }]);
