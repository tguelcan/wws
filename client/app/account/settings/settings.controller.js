'use strict';

angular.module('wwsApp')
  .controller('SettingsCtrl',['$scope', 'User', 'Location', 'Auth', '$location', function ($scope, User, Location, Auth, $location) {
    $scope.isAdmin = Auth.isAdmin;
    $scope.locations = {};
    $scope.errors = {};
    $scope.member = {};

    //CHANGE PASSWORT
    $scope.changePassword = function(form) {
      $scope.submitted = true;
      if(form.$valid) {
        Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
        .then( function() {
          $scope.message = 'Passwort wurde erfolgreich geändert.';
        })
        .catch( function() {
          form.password.$setValidity('mongoose', false);
          $scope.errors.other = 'Falsches passwort';
          $scope.message = '';
        });
      }
		};

    //LOCATION
    Location.get(function (locations) {
    }).$promise.then(function (locations) {
      $scope.locations = locations;
    }, function (error) {
    });

    $scope.reset = function(){
      $scope.member.location = "";
    };

    //CHANGE LOCATION

    $scope.changeLocation = function(location){
      //LOCATION
      User.changeLocation({id: location._id},function () {
      }).$promise.then(function () {
        Auth.logout();
        $location.path('/login');
      }, function (error) {
      });
    }
  }]);
