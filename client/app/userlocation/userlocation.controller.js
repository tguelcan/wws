'use strict';

angular.module('wwsApp')
  .controller('UserlocationCtrl',['$scope', '$stateParams', 'Location', 'Resource', 'User', 'socket', function ($scope, $stateParams, Location, Resource, User, socket) {
    $scope.location = {};
    $scope.LocationId = $stateParams.id;

    $scope.location = {};
    $scope.LocationId = $stateParams.id;
    $scope.search = function (searchText){
      if (searchText || searchText == "") {
        $scope.searchText = searchText;
        getResource();
      }
    };

    User.get({id: 'me'},function(){}).$promise.then(function (user) {
      $scope.isAdmin = (user.role == 'admin');
    }, function (error) {
      console.log(error);
    });


    //GET

    function getLocation(){
      Location.getOne({id:$scope.LocationId},function () {
      }).$promise.then(function (location) {
        $scope.location = location;

        $scope.marker = {
          id: 0,
          coords: {
            latitude: $scope.location.lat,
            longitude: $scope.location.long
          }
        };
        $scope.map = {center: {latitude: $scope.location.lat, longitude: $scope.location.long}, zoom: 13};
      }, function (error) {
        console.log(error);
      });
    }
    getLocation();


    Resource.getLocation({id: $scope.LocationId}, function (resources) {
    }).$promise.then(function (resources) {
      $scope.resources = resources;
      if(!$scope.resources.allquotas){$scope.resources.allquotas = 0;}
      if (!$scope.resources.allcounts){$scope.resources.allcounts = 0;}
      socket.syncUpdates('resource', $scope.resources.resources);
    }, function (error) {
      console.log(error);
    });



    $scope.lock = true;

    $scope.unlock = function(resource){
       saveResource(resource);
    };


    $scope.minus = function(resource) {
      if(!resource.count){
        return
      }
      resource.count --;
    };

    $scope.plus = function(resource) {
      if(resource.count == resource.quota){
        return
      }
      resource.count ++;
    };

    var saveResource = function(data){
      Resource.update(data, function () {
      }).$promise.then(function () {
        socket.syncUpdates('resource', $scope.resources.resources);
      }, function (error) {
        console.log(error);
      });
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('resource');
    });
  }]);

