'use strict';

describe('Controller: UserlocationCtrl', function () {

  // load the controller's module
  beforeEach(module('wwsApp'));

  var UserlocationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserlocationCtrl = $controller('UserlocationCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
