'use strict';

angular.module('wwsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('userlocation', {
        url: '/userlocation/:id',
        templateUrl: 'app/userlocation/userlocation.html',
        controller: 'UserlocationCtrl',
        authenticate: true
      });
  });
