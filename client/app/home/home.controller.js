'use strict';

angular.module('wwsApp')
  .controller('HomeCtrl',['$scope', 'Info', 'socket', function ($scope, Info, socket) {
    $scope.infos = [];

    //GET
    $scope.currentPage = 1;
    var getInfos = function() {
      Info.get({page: $scope.currentPage}, function () {
      }).$promise.then(function (infos) {
        $scope.infos = infos;

        socket.syncUpdates('info', $scope.infos.infos, function (event, info, infos) {
          // This callback is fired after the comments array is updated by the socket listeners

          // sort the array every time its modified

          infos.sort(function (a, b) {
            if(a && b){
            a = new Date(a.added);
            b = new Date(b.added);
            return a > b ? -1 : a < b ? 1 : 0;
            }
          });

        }, function (error) {
          console.log(error);
        });
      });
    };
      //Execute getArticles function
      getInfos();

      //Change PAGE
      $scope.changePage = function () {
        getInfos();
        window.location.hash = '#top';
      };

      $scope.$on('$destroy', function () {
        socket.unsyncUpdates('info');
      });
  }]);
