'use strict';

angular.module('wwsApp')
  .factory('Article', function ($resource) {
      return $resource('/api/articles/:id/:controller', {
          id: '@_id'
        },
        {
          add: {
            method: 'POST'
          },
          get: {
            method: 'GET',
            isArray: false
          },
          getall: {
            method: 'GET',
            isArray: true,
            params: {
              controller:'getall'
            }
          },
          getOne: {
            method: 'GET',
            isArray: false
          },
          delete: {
            method: 'DELETE'
          },
          update: {
            method: 'PUT'
          }
        });
    });
