'use strict';

describe('Service: article.service', function () {

  // load the service's module
  beforeEach(module('wwsApp'));

  // instantiate service
  var articleService;
  beforeEach(inject(function (_articleService_) {
    articleService = _article.service_;
  }));

  it('should do something', function () {
    expect(!!articleService).toBe(true);
  });

});
