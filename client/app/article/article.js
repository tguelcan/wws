'use strict';

angular.module('wwsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('article', {
        url: '/articles',
        templateUrl: 'app/article/article.html',
        controller: 'ArticleCtrl'
      })
      .state('articleDetail', {
        url: '/articles/:id',
        templateUrl: 'app/article/articleDetail.html',
        controller: 'ArticleActionCtrl'
      });
  });
