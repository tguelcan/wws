'use strict';

angular.module('wwsApp')
  .controller('ArticleCtrl',['$scope', 'Article', 'socket', 'Modal', '$location', '$modal', function ($scope, Article, socket, Modal, $location, $modal) {
    $scope.articles = [];

    $scope.search = function (searchText){
      if (searchText || searchText == "") {
        $scope.searchText = searchText;
        getArticles();
      }
    };

    //------------- MODAL -------------//

    //NEW Article to place
    $scope.addArticleModal = function () {
      $modal.open({
        templateUrl: 'app/article/articleAdd.html',
        size: 'lg',
        controller: 'ArticleModalCtrl'
      });
    };

    //GET
    $scope.currentPage = 1;
    var getArticles = function(){
      Article.get({page: $scope.currentPage, search: $scope.searchText},function () {
      }).$promise.then(function (articles) {
        $scope.articles = articles;
        socket.syncUpdates('article', $scope.articles.articles);
      }, function (error) {
        console.log(error);
      });
    };

    //Execute getArticles function
    getArticles();

    //Change PAGE
    $scope.changePage = function () {
      getArticles();
      window.location.hash = '#top';
    };



    //GOTO Article Detail Page
    $scope.goToDetail = function (article) {
      $location.url("/articles/" + article._id);
    };

    //GOTO New Article
    $scope.goToNew = function () {
      $location.url("/articlesAdd");
    };


    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('article');
    });
  }])


  .controller('ArticleModalCtrl',['$scope', 'Article', '$location', '$modalInstance', function ($scope, Article, $location, $modalInstance) {

    $scope.ok = function (article) {
      Article.add(article, function () {
      }).$promise.then(function () {
        $modalInstance.close();
        $location.url("/articles");
      }, function (error) {
        console.log(error);
      });

    };

  }])


  .controller('ArticleActionCtrl',['$scope', 'Article', 'Resource', 'Modal', '$modal', '$location', '$stateParams', 'User', 'socket', function ($scope, Article, Resource, Modal, $modal, $location, $stateParams, User, socket) {
    $scope.ArticleId = $stateParams.id;
    $scope.resources = [];

    User.get({id: 'me'}, function () {
    }).$promise.then(function (user) {
      $scope.isAdmin = (user.role == 'admin');
    }, function (error) {
      console.log(error);
    });

    //GET

    Article.getOne({id: $scope.ArticleId}, function (article) {
    }).$promise.then(function (article) {
      $scope.article = article;
    }, function (error) {
      console.log(error);
    });


    Resource.getArticle({id: $scope.ArticleId}, function (resources) {
    }).$promise.then(function (resources) {
      $scope.resources = resources;
      if(!$scope.resources.allquotas){$scope.resources.allquotas = 0;}
      if (!$scope.resources.allcounts){$scope.resources.allcounts = 0;}
      socket.syncUpdates('resource', $scope.resources.resources);
    }, function (error) {
      console.log(error);
    });

    //DELETE

    $scope.delete = Modal.confirm.delete(function (article) {
          Article.delete({id: article._id}, function () {})
            .$promise.then(function () {
            $location.url("/articles");
          }, function (error) {
            console.log(error);
          });

    });

    //EDIT

    $scope.editArticle = function (article) {
      Article.update({id: $scope.ArticleId}, article)
        .$promise.then(function () {
        $scope.editMode = false;
      }, function (error) {
        console.log(error);
      });
    };

    //ADD Resource

    //NEW Resource
    $scope.addResourceModal = function (param) {
      $modal.open({
        templateUrl: 'app/main/resourceAdd.html',
        size: 'lg',
        controller: 'ResourceModalCtrl',
        resolve: {
          param: function () {
            return param;
          }
        }
      });
    };


    //DELETE Resource

    $scope.deleteResource = Modal.confirm.delete(function (resource) { // callback when modal is confirmed
      Resource.delete({id: resource._id}, function () {
      }).$promise.then(function (resource) {
        socket.syncUpdates('resource',  $scope.resources.resources)
      }, function (error) {
        console.log(error);
      });
    });



    //GOTO New Article
    $scope.goToNew = function () {
      $location.url("/articles_add");
    };

    //GOTO Location Details
    $scope.goToLocationDetail = function (location) {
      $location.url("/locations/" + location._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('article');
    });

  }]);

