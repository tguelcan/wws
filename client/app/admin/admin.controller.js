'use strict';

angular.module('wwsApp')
  .controller('AdminCtrl',['$scope', '$http', 'Auth', 'User', 'Modal', '$modal', 'socket', function ($scope, $http, Auth, User, Modal, $modal, socket) {

    $scope.search = function (searchText) {
      if (searchText || searchText == "") {
        $scope.searchText = searchText;
        getUsers();
      }
    };

    $scope.currentPage = 1;
    var getUsers = function () {
      User.show({page: $scope.currentPage, search: $scope.searchText}, function () {
      }).$promise.then(function (users) {
        $scope.users = users;
        socket.syncUpdates('user', $scope.users.users);
      }, function (error) {
        console.log(error);
      });
    };
    getUsers();


    //Change PAGE
    $scope.changePage = function () {
      getUsers();
      window.location.hash = '#top';
    };


    $scope.delete = Modal.confirm.delete(function (user) { // callback when modal is confirmed
      User.remove({id: user._id})
        .$promise.then(function () {
        getUsers();
      }, function (error) {
        console.log(error);
      });
    });

    $scope.activeUser = function (user) {

      User.changeStatus({id: user._id}, {},
        function (cb) {
          if (cb.$promise) {
            socket.syncUpdates('user', $scope.users.users);
          }
        });

    };

    $scope.adminUser = function (user) {

      User.changeRole({id: user._id}, {},
        function (cb) {
          if (cb) {
            socket.syncUpdates('user', $scope.users.users);
          }
        });

    };


    //----- MODAL
    $scope.addUserModal = function () {
      $modal.open({
        templateUrl: 'app/admin/userAdd.html',
        size: 'lg',
        controller: 'UserAddModalCtrl'
      });
    };

    $scope.editUserModal = function (param) {
      $modal.open({
        templateUrl: 'app/admin/userAdd.html',
        size: 'lg',
        controller: 'UserEditModalCtrl',
        resolve: {
          param: function () {
            return param;
          }
        }
      });
    };


    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('user');
    });

  }])
  .controller('UserAddModalCtrl',['$scope', 'User', '$location', '$modalInstance', function ($scope, User, $location, $modalInstance) {

    $scope.user = {};
    $scope.errors = {};

    $scope.register = function (user) {
      $scope.submitted = true;

      User.addUser(user, function () {
      }).$promise.then(function () {
        $modalInstance.close();
      }, function (error) {
        console.log(error);
      });

    };

  }])
  .controller('UserEditModalCtrl',['$scope', 'User', '$location', '$modalInstance', 'param', function ($scope, User, $location, $modalInstance, param) {

    $scope.user = param;
    $scope.errors = {};

    $scope.register = function (user) {
      console.log(user);
      $scope.submitted = true;

      User.editUser(user, function () {
      }).$promise.then(function () {
        $modalInstance.close();
      }, function (error) {
        console.log(error);
      });

    };

  }]);
