'use strict';

angular.module('wwsApp')
  .factory('Info', function ($resource) {
      return $resource('/api/infos/:id/:controller', {
          id: '@_id'
        },
        {
          add: {
            method: 'POST'
          },
          get: {
            method: 'GET',
            isArray: false
          },
          delete: {
            method: 'DELETE'
          },
          update: {
            method: 'PUT'
          }
        });
    });
