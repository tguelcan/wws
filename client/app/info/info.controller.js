'use strict';

angular.module('wwsApp')
  .controller('InfoCtrl',['$scope', 'Info', 'socket', 'Modal', '$location', '$modal', function ($scope, Info, socket, Modal, $location, $modal) {
    $scope.infos = [];

    //------------- MODAL -------------//

    //NEW Article to place
    $scope.addInfoModal = function (information) {
      $modal.open({
        templateUrl: 'app/info/infoAdd.html',
        size: 'lg',
        controller: 'InfoModalCtrl',
        resolve: {
          information: function () {
            return null;
          }
        }
      });
    };


    //------------- MODAL -------------//

    //NEW Article to place
    $scope.editInfoModal = function (information) {
      $modal.open({
        templateUrl: 'app/info/infoAdd.html',
        size: 'lg',
        controller: 'InfoModalCtrl',
        resolve: {
          information: function () {
            return information;
          }
        }
      });
    };

    //GET
    $scope.currentPage = 1;
    var getInfos = function(){
      Info.get({page: $scope.currentPage},function () {
      }).$promise.then(function (infos) {
        $scope.infos = infos;
        socket.syncUpdates('info', $scope.infos.infos);
      }, function (error) {
        console.log(error);
      });
    };

    //Execute getArticles function
    getInfos();

    //Change PAGE
    $scope.changePage = function () {
      getInfos();
      window.location.hash = '#top';
    };



    //DELETE Resource

    $scope.deleteInfo = Modal.confirm.delete(function (info) { // callback when modal is confirmed
      Info.delete({id: info._id}, function () {
      }).$promise.then(function () {
        socket.syncUpdates('info',  $scope.infos.infos)
      }, function (error) {
        console.log(error);
      });
    });



    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('info');
    });
  }])
  .controller('InfoModalCtrl', ['$scope', 'Info', '$location', '$modalInstance', 'information', function ($scope, Info, $location, $modalInstance, information) {
    $scope.info = {};

    //CATEGORY

    $scope.data = {
      category: null,
      path: 'assets/images/icons/',
      availableOptions: [
        {id: 'backpack', name: 'Schule'},
        {id: 'bill', name: 'Rechnungen'},
        {id: 'bus', name: 'Schulbus'},
        {id: 'calc', name: 'Rechnen'},
        {id: 'candy', name: 'Süßigkeiten'},
        {id: 'car', name: 'Auto'},
        {id: 'chalkboard', name: 'Tafel'},
        {id: 'eye', name: 'Auge'},
        {id: 'hamburger', name: 'Hamburger'},
        {id: 'hotdog', name: 'Hotdog'},
        {id: 'map', name: 'Karte'},
        {id: 'medicine', name: 'Medizin'},
        {id: 'muffin', name: 'Muffin'},
        {id: 'openletter', name: 'Brief'},
        {id: 'paperplane', name: 'Papierflieger'},
        {id: 'pizza', name: 'Pizza'},
        {id: 'toaster', name: 'Toaster'},
        {id: 'www', name: 'Internet'}
      ]
    };

    if (information){
      $scope.info = information;

      $scope.data.repeatSelect = information.category;

      $scope.ok = function (info) {
        info.category = $scope.data.repeatSelect;
        Info.update(info, function () {
        }).$promise.then(function () {
          $modalInstance.close();
          $location.url("/info");
        }, function (error) {
          console.log(error);
        });
      };
    } else {

    $scope.ok = function (info) {
      info.category = $scope.data.repeatSelect;
      Info.add(info, function () {
      }).$promise.then(function () {
        $modalInstance.close();
        $location.url("/info");
      }, function (error) {
        console.log(error);
      });
    };
    }


  }]);

