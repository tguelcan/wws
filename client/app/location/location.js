'use strict';

angular.module('wwsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('location', {
        url: '/locations',
        templateUrl: 'app/location/location.html',
        controller: 'LocationCtrl',
        authenticate: true
      })
      .state('locationDetail', {
      url: '/locations/:id',
      templateUrl: 'app/location/locationDetail.html',
      controller: 'LocationActionCtrl',
      authenticate: true
      });
  });
