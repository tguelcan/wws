'use strict';

describe('Service: location.service', function () {

  // load the service's module
  beforeEach(module('wwsApp'));

  // instantiate service
  var locationService;
  beforeEach(inject(function (_locationService_) {
    locationService = _locationService_;
  }));

  it('should do something', function () {
    expect(!!location.service).toBe(true);
  });

});
