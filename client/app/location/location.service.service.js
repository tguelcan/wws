'use strict';

angular.module('wwsApp')
  .factory('Location', function ($resource) {
    return $resource('/api/locations/:id/:controller', {
        id: '@_id'
      },
      {
        add: {
          method: 'POST',
        },
        get: {
          method: 'GET',
          isArray: false
        },
        getOne: {
          method: 'GET',
          isArray: false
        },
        getall: {
          method: 'GET',
          isArray: true,
          params: {
            controller:'getall'
          }
        },
        delete: {
          method: 'DELETE'
        },
        update: {
          method: 'PUT'
        }
      });
  });
