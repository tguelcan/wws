'use strict';

angular.module('wwsApp')
  .controller('LocationCtrl',['$scope', 'Location', 'socket', 'Modal', '$modal', '$location', function ($scope, Location, socket, Modal, $modal, $location) {
    $scope.locations = [];

    $scope.search = function (searchText){
      if (searchText || searchText == "") {
        $scope.searchText = searchText;
        getLocations();
      }
    };

    //GET
    $scope.currentPage = 1;
    function getLocations(){
      Location.get({page: $scope.currentPage, search: $scope.searchText},function (locations) {
      }).$promise.then(function (locations) {
        $scope.locations = locations;
        socket.syncUpdates('location', $scope.locations.locations);
      }, function (error) {
        console.log(error);
      });
    }
    //Exec Locations
    getLocations();

    //Change PAGE
    $scope.changePage = function () {
      getLocations();
      window.location.hash = '#top';
    };



    //------------- MODAL -------------//

    //NEW Article to place
    $scope.addLocationModal = function () {
      $modal.open({
        templateUrl: 'app/location/locationAdd.html',
        size: 'lg',
        controller: 'LocationModalCtrl'
      });
    };


    //MAP Field
    $scope.options = {scrollwheel: false};

    //-------------------------------//


    //GOTO Article Detail Page
    $scope.goToDetail = function (location) {
      $location.url("/locations/" + location._id);
    };

    //GOTO New Article
    $scope.goToNew = function () {
      $location.url("/articlesAdd");
    };


    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('locations');
    });
  }])
  .controller('LocationActionCtrl',['$scope', 'Location', 'Resource', 'socket', 'Modal', '$location', '$stateParams', 'User', function ($scope, Location, Resource, socket, Modal, $location, $stateParams, User) {
    $scope.location = {};
    $scope.LocationId = $stateParams.id;

    $scope.search = function (searchText){
      if (searchText || searchText == "") {
        $scope.searchText = searchText;
        getResources();
      }
    };

    User.get({id: 'me'},function(){}).$promise.then(function (user) {
      $scope.isAdmin = (user.role == 'admin');
    }, function (error) {
      console.log(error);
    });


    //GET

    function getLocation(){
      Location.getOne({id:$scope.LocationId},function () {
      }).$promise.then(function (location) {
        $scope.location = location;

        $scope.marker = {
          id: 0,
          coords: {
            latitude: $scope.location.lat,
            longitude: $scope.location.long
          }
        };
        $scope.map = {center: {latitude: $scope.location.lat, longitude: $scope.location.long}, zoom: 13};
      }, function (error) {
        console.log(error);
      });
    }
    getLocation();



    //DELETE

    $scope.delete = Modal.confirm.delete(function (location) {
          Location.delete({id: location._id}, function (){})
            .$promise.then(function () {
            $location.url("/locations");
          }, function (error) {
            console.log(error);
          });
    });

    //EDIT

    $scope.editLocation = function(location){
      Location.update({ id: $scope.LocationId }, location)
        .$promise.then(function () {
        $scope.editMode = false;
        getLocation();
      }, function (error) {
        console.log(error);
      });
    };

    function getResources(){
    Resource.getLocation({id: $scope.LocationId, page: $scope.currentPage, search: $scope.searchText}, function (resources) {
    }).$promise.then(function (resources) {
      $scope.resources = resources;
      if(!$scope.resources.allquotas){$scope.resources.allquotas = 0;}
      if (!$scope.resources.allcounts){$scope.resources.allcounts = 0;}
      socket.syncUpdates('resource', $scope.resources.resources);
    }, function (error) {
      console.log(error);
    });
    }
    getResources();

    //DELETE Resource

    $scope.deleteResource = Modal.confirm.delete(function (resource) { // callback when modal is confirmed
      Resource.delete({id: resource._id}, function () {
      }).$promise.then(function (resource) {
        socket.syncUpdates('resource',  $scope.resources.resources)
      }, function (error) {
        console.log(error);
      });
    });

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('location');
      socket.unsyncUpdates('resource');
    });
  }])

  .controller('LocationModalCtrl',['$scope', 'Location', '$modalInstance', function ($scope, Location, $modalInstance) {
    $scope.location = {};
    //Map Input
    $scope.ok = function (location) {
      Location.add(location, function () {
      }).$promise.then(function () {
        $modalInstance.close();
      }, function (error) {
        console.log(error);
      });

    };

  }]);
